<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommonController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\LocalloginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	// Retrieve a piece of data from the session...
    $value = session('key');
	//$request->session()->put('key', 'value');
	//$data = $request->session()->all();

	$name = "testname";
 
    // Specifying a default value...
    //$value = session('key', 'default');
 
    // Store a piece of data in the session...
    //session(['key' => 'Test User']);

	$out = new \Symfony\Component\Console\Output\ConsoleOutput();
	$out->writeln("Value from session");
	$out->writeln($value);

    return view('welcome');
});

/*Route::get('/cas', function () {
	
	if(cas()->isAuthenticated()){
		return view('index');
	}else{
		cas()->authenticate();
    	return view('index');
	};
});*/

Route::get('/cas', [CommonController::class, 'isLoggedIn'], function(){
	
	if(!Session::has('username')){
		return redirect()->route('localLogin');
	}
});

Route::get('/localLogin', 'LocalloginController@login')->name('localLogin');

Route::get('/logout', [LogoutController::class, 'perform'])->name('logout.perform');