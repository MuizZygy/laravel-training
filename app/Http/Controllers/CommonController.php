<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CommonController extends Controller
{	
    public function isLoggedIn(){
		   if(Session::has('username')) {
    	   		return view('index');
			}else{
				cas()->authenticate();
				Session::put('username',cas()->user());
				return view('index');
			}
	}
}
