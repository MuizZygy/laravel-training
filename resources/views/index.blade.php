<?php
	use Illuminate\Support\Facades\Session;
	
	$id = Session::get('username');
   
   $out = new \Symfony\Component\Console\Output\ConsoleOutput();
   $out->writeln("Value from session");
   foreach (cas()->getAttributes() as $key => $value) {
    $out->writeln($key +" => "+ $value);
   }
   
   $out->writeln(cas()->user());
   $out->writeln(cas()->getAttribute('uid'));
   $out->writeln($id);
  
?>
<html>
<head>
<title>Treasury - Sample PHP Application Main Page</title>
</head>
<body>
<div style="width: 100%; text-align: center;">
<div style="text-align: right;"><a href="{{ route('logout.perform') }}" class="btn btn-outline-light me-2">Logout</a></div>
<h1 style="text-align: center;">Treasury - Sample PHP Application Main Page</h1>
<h3 style="text-align: center;"><?php echo "Welcome " . $id . "!"; ?></h3>
<p><span style="text-align: center;">You should only see this if you
have already logged in!</span></p>
<p>You can logout by clicking on the 'logout' link at the top right of
this page.</p>
</div>
</body>
</html>
